# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Add categories
categories = {
  guitars: [
    {
      name: "Гитары"
      subs: [
        { name: "Акустические гитары" },
        { name: "Бас-гитары" },
        { name: "Электрогитары" }
      ]
    }
  ],
  drums: [
    {
      name: "Ударные"
      subs: [
        { name: "Акустические ударные" },
        { name: "Электронные ударные" },
        { name: "Аксессуары" }
      ]
    }
  ],
  studio: [
    {
      name: "Студия"
      subs: [
        { name: "Звуковые карты" },
        {
          name: "Микрофоны"
          subs: [
            { name: "Динамические" },
            { name: "Конденсаторные" },
            { name: "Ленточные" },
          ]
        },
        { name: "Студийные мониторы" }
      ]
    }
  ]
}

categories.each do |key, category|
  
end