class AddProductToPropValues < ActiveRecord::Migration[5.2]
  def change
    add_reference :prop_values, :product, foreign_key: true
  end
end
