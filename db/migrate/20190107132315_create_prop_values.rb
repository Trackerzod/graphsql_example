class CreatePropValues < ActiveRecord::Migration[5.2]
  def change
    create_table :prop_values do |t|
      t.string :prop_value

      t.timestamps
    end
  end
end
