class AddPropertyToPropValues < ActiveRecord::Migration[5.2]
  def change
    add_reference :prop_values, :property, foreign_key: true
  end
end
