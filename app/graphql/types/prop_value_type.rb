module Types
    class PropValueType < Types::BaseObject
        field :id, ID, null: false
        field :prop_value, String, null: false
        field :product, Types::ProductType, null: true
        field :property, Types::PropertyType, null: true
    end
end