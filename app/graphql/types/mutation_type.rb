module Types
  class MutationType < Types::BaseObject
    field :updatePropValue, mutation: Mutations::UpdatePropValue
    field :createPropValue, mutation: Mutations::CreatePropValue
    field :createProperty, mutation: Mutations::CreateProperty
    field :updateProperty, mutation: Mutations::UpdateProperty
    field :deleteProperty, mutation: Mutations::DeleteProperty
    
    field :createCategory, mutation: Mutations::CreateCategory
    field :updateCategory, mutation: Mutations::UpdateCategory
    field :deleteCategory, mutation: Mutations::DeleteCategory

    field :createProduct, mutation: Mutations::CreateProduct
    field :updateProduct, mutation: Mutations::UpdateProduct
    field :deleteProduct, mutation: Mutations::DeleteProduct

    # TODO: remove me
    # field :test_field, String, null: false,
    #   description: "An example field added by the generator"
    # def test_field
    #   "Hello World"
    # end
  end
end
