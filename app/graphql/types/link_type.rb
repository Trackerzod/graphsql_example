module Types
  class LinkType < Types::BaseObject
    field :id, ID, null: true
    field :url, String, null: true
    field :description, String, null: true
  end
end