module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.
    # include Queries::Categories
    
    # Links fields
    field :allLinks, [Types::LinkType], null: true
    field :link, Types::LinkType, null: true do
      argument :id, ID, required: true
    end

    # Categories fields
    field :categories, [Types::CategoryType], null: true do
      description "Get all categories"
    end
    field :category, Types::CategoryType, null: true do
      description "Get single category"
      argument :id, ID, required: true
    end
    field :category_tree, [Types::CategoryType], null: true, description: "Get category tree"
    
    # Products fields
    field :products, [Types::ProductType], null: true, description: "Get all products"
    field :product, Types::ProductType, null: true do
      argument :id, ID, required: true
    end

    # Properties fields
    field :properties, [Types::PropertyType], null: true, description: "Get all product properties"
    field :property, Types::PropertyType, null: true do
      description "Get property by id"
      argument :id, ID, required: true
    end

    # PropValues fields
    field :prop_values, [Types::PropValueType], null: true, description: "Get all property values"
    field :prop_value, Types::PropValueType, null: true do
      description "Get property value by id"
      argument :id, ID, required: false
    end 

    # Links methods
    def all_links
      Link.all
    end

    def link(id:)
      Link.find(id)
    end

    # Categories methods
    def categories
      Category.all
    end

    def category(id:)
      Category.find(id)
    end

    def category_tree
      Category.where(parent: nil)
    end

    # Product methods
    def products
      Product.all
    end

    def product(id:)
      Product.find(id)
    end

    # Properties methods
    def properties
      Property.all
    end

    def property(id:)
      Property.find(id)
    end

    # PropValues methods
    def prop_values
      PropValue.all
    end

    def prop_value(id:)
      PropValue.find(id)
    end
  end
end