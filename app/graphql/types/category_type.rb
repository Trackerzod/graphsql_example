module Types
  class CategoryType < Types::BaseObject
    field :id, ID, null: true
    field :name, String, null: true
    field :slug, String, null: true
    field :parent, Types::CategoryType, null: true
    field :children, [Types::CategoryType], null: true
    field :products, [Types::ProductType], null: true
  end
end
