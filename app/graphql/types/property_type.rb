module Types
    class PropertyType < Types::BaseObject
        field :id, ID, null: false
        field :name, String, null: false
        field :products, [Types::ProductType], null: true
        field :prop_values, [Types::PropValueType], null: true        
    end
end