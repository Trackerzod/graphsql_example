module Types
  class ProductQueryType < Types::QueryType
    field :allProducts, [Types::ProductType], null: true, description: "Get all products"
    field :product, Types::ProductType, null: true do
      argument :id, ID, required: true
    end

    def all_products
      Product.all
    end

    def product(id:)
      Product.find(id)
    end
  end
end
