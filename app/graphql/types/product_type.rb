module Types
  class ProductType < Types::BaseObject
    field :id, ID, null: false
    field :count, Integer, null: true
    field :description, String, null: true
    field :name, String, null: false
    field :price, Float, null: true
    field :slug, String, null: true
    field :category, Types::CategoryType, null: true
    field :properties, [Types::PropertyType], null: true
    field :prop_values, [Types::PropValueType], null: true    
  end
end
