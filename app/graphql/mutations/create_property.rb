module Mutations
  class CreateProperty < Mutations::BaseMutation
    # TODO: define return fields
    # field :post, Types::PostType, null: false
    field :property, Types::PropertyType, null: false
    field :errors, [String], null: true

    # TODO: define arguments
    # argument :name, String, required: true
    argument :name, String, required: true
    argument :product_ids, [ID], required: false
    argument :property_ids, [ID], required: false

    # TODO: define resolve method
    # def resolve(name:)
    #   { post: ... }
    # end

    def resolve(args)
      property = Property.new(args)
      if property.save
        {
          property: property,
          errors: nil
        }
      else
        {
          property: nil,
          errors: property.errors.full_messages
        }
      end
    end
  end
end
