module Mutations
  class CreateCategory < Mutations::BaseMutation
    # TODO: define return fields
    # field :post, Types::PostType, null: false
    field :category, Types::CategoryType, null: false
    field :errors, [String], null: true

    # TODO: define arguments
    # argument :name, String, required: true
    argument :name, String, required: true
    argument :parent_id, ID, required: false

    # TODO: define resolve method
    # def resolve(name:)
    #   { post: ... }
    # end

    def resolve(args)
      category = Category.new(args)
      if category.save 
        {
          category: category,
          errors: []
        }
      else
        {
          category: nil,
          errors: category.errors.full_messages
        }
      end
    end
  end
end
