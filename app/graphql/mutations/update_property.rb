module Mutations
  class UpdateProperty < Mutations::BaseMutation
    # TODO: define return fields
    # field :post, Types::PostType, null: false
    field :property, Types::PropertyType, null: false
    field :errors, [String], null: true

    # TODO: define arguments
    # argument :name, String, required: true
    argument :id, ID, required: true
    argument :name, String, required: false
    argument :product_ids, [ID], required: false
    # argument :prop_value_ids, [ID], required: false

    # TODO: define resolve method
    # def resolve(name:)
    #   { post: ... }
    # end

    def resolve(args)
      property = Property.find(args[:id])
      if property.update(args)
        {
          property: property,
          errors: nil
        }
      else
        {
          property: nil,
          errors: property.errors.full_messages
        }
      end
    end
  end
end
