module Mutations
  class UpdateProduct < Mutations::BaseMutation
    # TODO: define return fields
    # field :post, Types::PostType, null: false
    field :product, Types::ProductType, null: false
    field :errors, [String], null: true

    # TODO: define arguments
    # argument :name, String, required: true'
    argument :id, ID, required: true
    argument :name, String, required: true
    argument :slug, String, required: false
    argument :price, Float, required: false
    argument :count, Integer, required: false
    argument :description, String, required: false
    argument :category_id, ID, required: false

    # TODO: define resolve method
    # def resolve(name:)
    #   { post: ... }
    # end

    def resolve(args)
      product = Product.find(args[:id])

      if product.update(args)
        {
          product: product,
          errors: []
        }
      else
        {
          product: nil,
          errors: product.errors.full_messages
        }
      end
    end
  end
end
