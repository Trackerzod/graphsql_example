module Mutations
  class DeleteProduct < Mutations::BaseMutation
    # TODO: define return fields
    field :product, Types::ProductType, null: true
    field :errors, [String], null: true

    # TODO: define arguments
    argument :id, ID, required: true

    # TODO: define resolve method
    def resolve(args)
      product = Product.find(args[:id])
      if product.delete
        {
          product: nil,
          errors: nil
        }
      else
        {
          product: nil,
          errors: product.errors.full_messages
        }
      end
    end
  end
end
