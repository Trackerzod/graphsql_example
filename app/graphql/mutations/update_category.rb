module Mutations
  class UpdateCategory < Mutations::BaseMutation
    # TODO: define return fields
    # field :post, Types::PostType, null: false
    field :category, Types::CategoryType, null: false
    field :errors, [String], null: true

    # TODO: define arguments
    # argument :name, String, required: true'
    argument :id, ID, required: true
    argument :name, String, required: false
    argument :slug, String, required: false
    argument :parent_id, ID, required: false
    argument :product_ids, [ID], required: false

    # TODO: define resolve method
    # def resolve(name:)
    #   { post: ... }
    # end

    def resolve(args)
      category = Category.find(args[:id])

      if category.update(args)
        {
          category: category,
          errors: []
        }
      else
        {
          category: nil,
          errors: category.errors.full_messages
        }
      end
    end
  end
end
