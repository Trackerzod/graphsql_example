module Mutations
  class CreateProduct < Mutations::BaseMutation
    # TODO: define return fields
    # field :post, Types::PostType, null: false
    field :product, Types::ProductType, null: false
    field :errors, [String], null: true

    # TODO: define arguments
    # argument :name, String, required: true
    argument :name, String, required: true
    argument :price, Float, required: false
    argument :count, Integer, required: false
    argument :description, String, required: false
    argument :category_id, ID, required: false
    argument :property_ids, [ID], required: false
    argument :prop_value_ids, [ID], required: false

    # TODO: define resolve method
    # def resolve(name:)
    #   { post: ... }
    # end

    def resolve(args)
      product = Product.new(args)
      if product.save 
        {
          product: product,
          errors: []
        }
      else
        {
          product: nil,
          errors: product.errors.full_messages
        }
      end
    end
  end
end
