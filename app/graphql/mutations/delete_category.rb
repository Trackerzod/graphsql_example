module Mutations
  class DeleteCategory < Mutations::BaseMutation
    # TODO: define return fields
    field :category, Types::CategoryType, null: true
    field :errors, [String], null: true

    # TODO: define arguments
    argument :id, ID, required: true

    # TODO: define resolve method
    def resolve(args)
      category = Category.find(args[:id])
      if category.delete
        {
          category: nil,
          errors: nil
        }
      else
        {
          category: nil,
          errors: category.errors.full_messages
        }
      end
    end
  end
end
