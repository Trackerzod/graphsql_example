module Mutations
  class CreatePropValue < Mutations::BaseMutation
    # TODO: define return fields
    # field :post, Types::PostType, null: false
    field :prop_value, Types::PropValueType, null: true
    field :errors, [String], null: true

    # TODO: define arguments
    argument :prop_value, String, required: true
    argument :product_id, ID, required: true
    argument :property_id, ID, required: true

    # TODO: define resolve method
    def resolve(args)
      prop_value = PropValue.new(args)
      if prop_value.save
        {
          prop_value: prop_value,
          errors: nil
        }
      else
        {
          prop_value: nil,
          errors: prop_value.errors.full_messages
        }
      end
    end
  end
end
