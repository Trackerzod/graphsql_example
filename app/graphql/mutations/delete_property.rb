module Mutations
  class DeleteProperty < Mutations::BaseMutation
    # TODO: define return fields
    field :property, Types::PropertyType, null: true
    field :errors, [String], null: true

    # TODO: define arguments
    argument :id, ID, required: true

    # TODO: define resolve method
    def resolve(args)
      property = Property.find(args[:id])
      if Property.delete
        {
          property: nil,
          errors: nil
        }
      else
        {
          property: nil,
          errors: property.errors.full_messages
        }
      end
    end
  end
end
