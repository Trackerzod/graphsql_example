module Mutations
  class UpdatePropValue < Mutations::BaseMutation
    # TODO: define return fields
    # field :post, Types::PostType, null: false
    field :prop_value, Types::PropValueType, null: true
    field :errors, [String], null: true

    # TODO: define arguments
    # argument :name, String, required: true
    argument :id, ID, required: true
    argument :prop_value, String, required: true
    argument :product_id, ID, required: false
    argument :property_id, ID, required: false

    # TODO: define resolve method
    def resolve(args)
      prop_value = PropValue.find(args[:id])
      if prop_value.update(args)
        {
          prop_value: prop_value,
          errors: nil
        }
      else
        {
          prop_value: nil,
          errors: prop_value.errors.full_messages
        }
      end
    end
  end
end
