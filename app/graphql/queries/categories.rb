module Queries
  module Categories
    extend ActiveSupport::Concern
    
    included do
      field :categories, [Types::CategoryType], null: true, description: "Get all categories"
      field :category, Types::CategoryType, null: true do
        argument :id, ID, required: true
      end
      field :category_tree, [Types::CategoryType], null: true, description: "Get category tree"
    end

    def categories
      Category.all
    end

    def category(id:)
      Category.find(id)
    end

    def category_tree
      Category.where(parent: nil)
    end
  end
end
