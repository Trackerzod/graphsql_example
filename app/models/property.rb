# == Schema Information
#
# Table name: properties
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Property < ApplicationRecord
    validates :name, presence: true

    has_many :product_properties
    has_many :products, through: :product_properties
    has_many :prop_values, dependent: :destroy
end