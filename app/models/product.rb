# == Schema Information
#
# Table name: products
#
#  id          :bigint(8)        not null, primary key
#  count       :integer
#  description :text
#  name        :string
#  price       :float
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint(8)
#
# Indexes
#
#  index_products_on_category_id  (category_id)
#

class Product < ApplicationRecord
    extend FriendlyId

    validates :name, presence: true
    validates :slug, uniqueness: true

    has_many :product_properties
    has_many :properties, through: :product_properties
    has_many :prop_values
    belongs_to :category, optional: true

    friendly_id :name, use: :slugged

    def in_stock?
        count > 0
    end
end
