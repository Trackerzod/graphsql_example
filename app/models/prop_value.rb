# == Schema Information
#
# Table name: prop_values
#
#  id          :bigint(8)        not null, primary key
#  prop_value  :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  product_id  :bigint(8)
#  property_id :bigint(8)
#
# Indexes
#
#  index_prop_values_on_product_id   (product_id)
#  index_prop_values_on_property_id  (property_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#  fk_rails_...  (property_id => properties.id)
#

class PropValue < ApplicationRecord
    validates :prop_value, presence: true

    belongs_to :product
    belongs_to :property
end
