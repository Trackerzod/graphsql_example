# == Schema Information
#
# Table name: categories
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  parent_id  :bigint(8)
#
# Indexes
#
#  index_categories_on_parent_id  (parent_id)
#

class Category < ApplicationRecord
    extend FriendlyId
    
    validates :name, presence: true
    validates :slug, uniqueness: true

    has_many :children, class_name: "Category", foreign_key: "parent_id"
    belongs_to :parent, class_name: "Category", required: false
    has_many :products, dependent: :nullify

    friendly_id :name, use: :slugged

    def descendents
        children.each do |child|
            [child] + child.descendents
        end
    end

end
